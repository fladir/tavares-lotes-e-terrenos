<?php
    namespace CiaWebsites\GetNewsLetterMail;

    require_once __DIR__ .'/xml/xml-exporter.php';

    function load_lib_xml($default){
        return new \XmlExporter($default);
    }