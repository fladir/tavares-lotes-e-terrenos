<?php
/* Template Name: Nossas Obras */
get_header();
$produtos = get_field('produtos');
$args = array(
    'nopaging'               => false,
    'paged'                  => $paged,
    'post_type' => 'obra',
    'posts_per_page' => 6,
    'orderby' => 'post_date',
    'order' => 'DESC'
);
$WPQuery = new WP_Query($args);

?>

<?php echo do_shortcode('[mdf_custom template=any/my_cars_2 post_type=imovel orderby=date order=desc page=1 per_page=9 pagination=tb]'); ?>

<?php get_footer(); ?>

