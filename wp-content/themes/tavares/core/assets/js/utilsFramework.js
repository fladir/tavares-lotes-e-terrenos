async function instanciarCarousel(seletor) {
    jQuery(seletor).owlCarousel({
        loop: false,
        margin: 0,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1,
                nav: true,
                navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>']
            },
            768: {
                items: 3,
                nav: true,
                navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>']
            },
            1000: {
                items: 4
            }
        }
    })
}

async function instanciarCarouselValores(seletor) {
    jQuery(seletor).owlCarousel({
        loop: false,
        margin: 5,
        nav: true,
        navText: ['<i class="fas fa-angle-left"></i>', '<i class="fas fa-angle-right"></i>'],
        dots: false,
        items: 1,
    })
}

async function instanciarPortfolioSlide(seletor){
    jQuery(seletor).owlCarousel({
        loop: true,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        autoplay: false,
        nav: false,
        dots: true,
        dotData: true,
        dotsData: true,
        items: 1,
    })
}

async function instanciarPortfolioSlideIndex(seletor){
    jQuery(seletor).owlCarousel({
        loop: true,
        autoplay: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        nav: false,
        dots: false,
        items: 1,
    })
}

async function dataMask(seletor = '.date', format = '00/00/0000') {
    jQuery(seletor).mask(format);
}
async function timeMask(seletor = '.date_time', format = '00/00/0000 00:00:00') {
    jQuery(seletor).mask(format);
}
async function cepMask(seletor = '.cep', format = '00000-000') {
    jQuery(seletor).mask(format);
}
async function phoneMask(seletor = '.phone', format = '(00) 0 0000-0000') {
    jQuery(seletor).mask(format);
}
async function cpfMask(seletor = '.cpf', format = '000.000.000-00') {
    jQuery(seletor).mask(format, { reverse: true });
}
async function cnpjMask(seletor = '.cnpj', format = '00.000.000/0000-00') {
    jQuery(seletor).mask(format, { reverse: true });
}
async function moneyMask(seletor = '.money', format = '#.##0,00') {
    jQuery(seletor).mask(format, { reverse: true });
}
