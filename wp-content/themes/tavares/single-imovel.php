<?php
get_header();
$tipoImovel = get_field( 'tipo_de_imovel' );
$destaque   = get_field( 'destaque' );
$term = get_queried_object();
?>
    <section class="title-and-breadcrumb pb-3">
        <div class="container">
            <div class="row">
                <div class="col-6">
					<?php
					if ( function_exists( 'yoast_breadcrumb' ) ) {
						yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
					}
					?>
                    <h1 class="single-imovel-title fw-medium">
						<?php
						$terms = get_the_terms( $post->ID, 'imoveis' );
						foreach ( $terms as $term ) {
							echo $term->name;
						}
						?>
                        |
						<?php
						$terms = get_the_terms( $post->ID, 'cidades' );
						foreach ( $terms as $term ) {
							echo $term->name;
						}
						?>
                    </h1>
                </div>
                <div class="col-md-6 d-flex align-items-end justify-content-end">
                    <h3 class="codigo-valor fw-medium">
                        CÓDIGO: <?php echo get_field( 'codigo' ) ?>
                        |
                        VALOR: R$ <?php echo get_field( 'preco' ) ?>
                    </h3>
                </div>
            </div>
        </div>
    </section>

    <div class="single_page_listing_style">
        <div class="container-fluid p0">
            <div class="row">


                <div class="row m0 single-imovel-carousel">


					<?php
					$url           = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ), 'imovel_full' );
					$galeriaImovel = get_field( 'galeria_imovel' );
					if ( $galeriaImovel ):
						?>
                        <div class="item">
                            <div class="spls_style_one">
                                <a data-fancybox="gallery" class="popup-img"
                                   href="<?php echo $url ?>">
									<?php the_post_thumbnail( 'carousel_slider_single_imovel' ); ?>
                                </a>
                            </div>
                        </div>
						<?php foreach ( $galeriaImovel as $item ): ?>

                        <div class="item">
                            <div class="spls_style_one">
                                <a data-fancybox="gallery" class="popup-img"
                                   href="<?php echo esc_url( $item['sizes']['imovel_full'] ); ?>">
                                    <img class="img-fluid w100"
                                         src="<?php echo esc_url( $item['sizes']['carousel_slider_single_imovel'] ); ?>">
                                </a>
                            </div>
                        </div>
					<?php
					endforeach; ?>

					<?php endif; ?>


                </div>


            </div>
        </div>
    </div>


    <!-- Agent Single Grid View -->


    <section class="our-agent-single bgc-f7 pb30-991">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="listing_sidebar dn db-991">
                        <div class="sidebar_content_details style3">
                            <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> -->
                            <div class="sidebar_listing_list style2 mobile_sytle_sidebar mb0">
                                <div class="sidebar_advanced_search_widget">
                                    <h4 class="mb25">Advanced Search <a class="filter_closed_btn float-right"
                                                                        href="#"><small>Hide Filter</small> <span
                                                    class="flaticon-close"></span></a></h4>
                                    <ul class="sasw_list style2 mb0">
                                        <li class="search_area">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputName1"
                                                       placeholder="keyword">
                                                <label for="exampleInputEmail"><span
                                                            class="flaticon-magnifying-glass"></span></label>
                                            </div>
                                        </li>
                                        <li class="search_area">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputEmail"
                                                       placeholder="Location">
                                                <label for="exampleInputEmail"><span
                                                            class="flaticon-maps-and-flags"></span></label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Status</option>
                                                        <option>Apartment</option>
                                                        <option>Bungalow</option>
                                                        <option>Condo</option>
                                                        <option>House</option>
                                                        <option>Land</option>
                                                        <option>Single Family</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Property Type</option>
                                                        <option>Apartment</option>
                                                        <option>Bungalow</option>
                                                        <option>Condo</option>
                                                        <option>House</option>
                                                        <option>Land</option>
                                                        <option>Single Family</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="small_dropdown2">
                                                <div id="prncgs" class="btn dd_btn">
                                                    <span>Price</span>
                                                    <label for="exampleInputEmail2"><span
                                                                class="fa fa-angle-down"></span></label>
                                                </div>
                                                <div class="dd_content2">
                                                    <div class="pricing_acontent">
                                                        <input type="text" class="amount" placeholder="$52,239">
                                                        <input type="text" class="amount2" placeholder="$985,14">
                                                        <div class="slider-range"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Bathrooms</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Bedrooms</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Garages</option>
                                                        <option>Yes</option>
                                                        <option>No</option>
                                                        <option>Others</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Year built</option>
                                                        <option>2013</option>
                                                        <option>2014</option>
                                                        <option>2015</option>
                                                        <option>2016</option>
                                                        <option>2017</option>
                                                        <option>2018</option>
                                                        <option>2019</option>
                                                        <option>2020</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="min_area style2 list-inline-item">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputName2"
                                                       placeholder="Min Area">
                                            </div>
                                        </li>
                                        <li class="max_area list-inline-item">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputName3"
                                                       placeholder="Max Area">
                                            </div>
                                        </li>
                                        <li>
                                            <div id="accordion" class="panel-group">
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a href="#panelBodyRating" class="accordion-toggle link"
                                                               data-toggle="collapse" data-parent="#accordion"><i
                                                                        class="flaticon-more"></i> Advanced features</a>
                                                        </h4>
                                                    </div>
                                                    <div id="panelBodyRating" class="panel-collapse collapse">
                                                        <div class="panel-body row">
                                                            <div class="col-lg-12">
                                                                <ul class="ui_kit_checkbox selectable-list float-left fn-400">
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck1">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck1">Air
                                                                                Conditioning</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck4">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck4">Barbeque</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck10">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck10">Gym</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck5">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck5">Microwave</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck6">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck6">TV Cable</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck2">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck2">Lawn</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck11">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck11">Refrigerator</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck3">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck3">Swimming
                                                                                Pool</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                                <ul class="ui_kit_checkbox selectable-list float-right fn-400">
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck12">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck12">WiFi</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck14">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck14">Sauna</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck7">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck7">Dryer</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck9">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck9">Washer</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck13">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck13">Laundry</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck8">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck8">Outdoor
                                                                                Shower</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input"
                                                                                   id="customCheck15">
                                                                            <label class="custom-control-label"
                                                                                   for="customCheck15">Window
                                                                                Coverings</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_button">
                                                <button type="submit" class="btn btn-block btn-thm">Search</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-8 mt50">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="the_title mb-5" style="top: -15px;">
                                DESCRIÇÃO
                            </h1>
                            <div style="height: 20px"></div>
							<?php the_content(); ?>
                        </div>
                        <div class="col-lg-12">

                            <div class="row">
                                <div class="col-lg-12 mt-4">
                                    <h1 class="the_title mb-5" style="top: -15px;">
                                        DETALHES
                                    </h1>
                                    <div style="height: 20px"></div>
                                </div>
                                <div style="height: 20px"></div>
								<?php
								$teste = get_field( 'caracteristicas_do_imovel' )['caracteristica_imovel'];
								#print_r($teste) ?>
                                <ul class="row order_list list-inline-item list-imovel">
                                    <li class="col-lg-6 col-sm-6 col-xs-12">
                                        <a>
                                            <img src="<?php bloginfo( 'template_directory' ); ?>/assets/img/check-imovel.png" class="mr-3"/>

                                            Área Construída: <?php echo get_field( 'area' ); ?>m²
                                        </a>
                                    </li>
                                    <li class="col-lg-6 col-sm-6 col-xs-12">
                                        <a>
                                            <img src="<?php bloginfo( 'template_directory' ); ?>/assets/img/check-imovel.png" class="mr-3"/>

                                            Cidade:
											<?php
											$terms = get_the_terms( $post->ID, 'cidades' );
											foreach ( $terms as $term ) {
												echo $term->name;
											}
											?>
                                        </a>
                                    </li>
                                    <li class="col-lg-6 col-sm-6 col-xs-12">
                                        <a>
                                            <img src="<?php bloginfo( 'template_directory' ); ?>/assets/img/check-imovel.png" class="mr-3"/>

                                            Bairro:
											<?php
											$terms = get_the_terms( $post->ID, 'bairros' );
											foreach ( $terms as $term ) {
												echo $term->name;
											}
											?>
                                        </a>
                                    </li>
                                    <li class="col-lg-6 col-sm-6 col-xs-12">
                                        <a>
                                            <img src="<?php bloginfo( 'template_directory' ); ?>/assets/img/check-imovel.png" class="mr-3"/>

											<?php
											$terms = get_the_terms( $post->ID, 'imoveis' );
											foreach ( $terms as $term ) {
												echo $term->name;
											}
											?>
                                        </a>
                                    </li>
									<?php
									foreach ( $teste as $tset ) {
										?>
                                        <li class="col-lg-6 col-sm-6 col-xs-12">
                                            <a>
                                                <img src="<?php bloginfo( 'template_directory' ); ?>/assets/img/check-imovel.png" class="mr-3"/>

												<?php echo $tset; ?>
                                            </a>
                                        </li>
										<?php
									}
									?>
                                </ul>
                            </div>

                        </div>
                        <div class="col-lg-12">
                            <style type="text/css">
                                .acf-map {
                                    width: 100%;
                                    height: 400px;
                                    border: #ccc solid 1px;
                                    margin: 20px 0;
                                }

                                // Fixes potential theme css conflict.
                                   .acf-map img {
                                       max-width: inherit !important;
                                   }
                            </style>
                            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxQHlF-7oXndl6laoEGZCzQ9kho7HhJzQ"></script>
                            <script type="text/javascript">
                                (function( $ ) {

                                    /**
                                     * initMap
                                     *
                                     * Renders a Google Map onto the selected jQuery element
                                     *
                                     * @date    22/10/19
                                     * @since   5.8.6
                                     *
                                     * @param   jQuery $el The jQuery element.
                                     * @return  object The map instance.
                                     */
                                    function initMap( $el ) {

                                        // Find marker elements within map.
                                        var $markers = $el.find('.marker');

                                        // Create gerenic map.
                                        var mapArgs = {
                                            zoom        : $el.data('zoom') || 16,
                                            mapTypeId   : google.maps.MapTypeId.ROADMAP
                                        };
                                        var map = new google.maps.Map( $el[0], mapArgs );

                                        // Add markers.
                                        map.markers = [];
                                        $markers.each(function(){
                                            initMarker( $(this), map );
                                        });

                                        // Center map based on markers.
                                        centerMap( map );

                                        // Return map instance.
                                        return map;
                                    }

                                    /**
                                     * initMarker
                                     *
                                     * Creates a marker for the given jQuery element and map.
                                     *
                                     * @date    22/10/19
                                     * @since   5.8.6
                                     *
                                     * @param   jQuery $el The jQuery element.
                                     * @param   object The map instance.
                                     * @return  object The marker instance.
                                     */
                                    function initMarker( $marker, map ) {

                                        // Get position from marker.
                                        var lat = $marker.data('lat');
                                        var lng = $marker.data('lng');
                                        var latLng = {
                                            lat: parseFloat( lat ),
                                            lng: parseFloat( lng )
                                        };

                                        // Create marker instance.
                                        var marker = new google.maps.Marker({
                                            position : latLng,
                                            map: map
                                        });

                                        // Append to reference for later use.
                                        map.markers.push( marker );

                                        // If marker contains HTML, add it to an infoWindow.
                                        if( $marker.html() ){

                                            // Create info window.
                                            var infowindow = new google.maps.InfoWindow({
                                                content: $marker.html()
                                            });

                                            // Show info window when marker is clicked.
                                            google.maps.event.addListener(marker, 'click', function() {
                                                infowindow.open( map, marker );
                                            });
                                        }
                                    }

                                    /**
                                     * centerMap
                                     *
                                     * Centers the map showing all markers in view.
                                     *
                                     * @date    22/10/19
                                     * @since   5.8.6
                                     *
                                     * @param   object The map instance.
                                     * @return  void
                                     */
                                    function centerMap( map ) {

                                        // Create map boundaries from all map markers.
                                        var bounds = new google.maps.LatLngBounds();
                                        map.markers.forEach(function( marker ){
                                            bounds.extend({
                                                lat: marker.position.lat(),
                                                lng: marker.position.lng()
                                            });
                                        });

                                        // Case: Single marker.
                                        if( map.markers.length == 1 ){
                                            map.setCenter( bounds.getCenter() );

                                            // Case: Multiple markers.
                                        } else{
                                            map.fitBounds( bounds );
                                        }
                                    }

// Render maps on page load.
                                    $(document).ready(function(){
                                        $('.acf-map').each(function(){
                                            var map = initMap( $(this) );
                                        });
                                    });

                                })(jQuery);
                            </script>
	                        <?php
	                        $location = get_field('mapa_imovel');

	                        if( $location ): ?>
                            <div class="col-lg-12 mt-4 p-0">
                                <h1 class="the_title mb-5" style="top: -15px;">
                                    LOCALIZAÇÃO
                                </h1>
                                <div style="height: 20px"></div>
                            </div>

                                <div class="acf-map" data-zoom="16">
                                    <div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
                                </div>
	                        <?php endif; ?>


                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-xl-4 mt50">


                    <div class="sidebar_listing_list" style="background-image: url(<?php bloginfo( 'template_directory' ); ?>/assets/img/bg-form-imovel.jpg); background-repeat: no-repeat;
                            background-color: #fe1032;">
                        <h3 class="text-center text-white text-uppercase mb-4">
                            Quero falar com
                            um Corretor
                        </h3>
                        <img src="<?php bloginfo( 'template_directory' ); ?>/assets/img/seta-form-imovel.png" class="pt-3 pb-4" style="margin: 0 auto; display: block"/>
						<?php echo do_shortcode( '[contact-form-7 title="Contato"]' ); ?>
                    </div>

                </div>
            </div>
        </div>
    </section>


    <!-- Simular Financiamento -->
<?php  get_template_part('components/imovel/simular-financiamento'); ?>

    <!-- Slider venda -->
<?php  get_template_part('components/index/oportunidades'); ?>
<?php get_footer(); ?>