<?php
/* Template Name: Quem Somos */
get_header();
?>
    <section class="title-and-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
					<?php
					if ( function_exists( 'yoast_breadcrumb' ) ) {
						yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
					}
					?>
                    <h1 class="the_title mb-4">
						<?php the_title() ?>
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- About Text Content -->
    <section class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-11 offset-1 text-center">
					<?php if ( have_posts() ) : while ( have_posts() ) :
					the_post(); ?>
					<?php the_content(); ?>
                </div>
				<?php endwhile;
				endif;
				wp_reset_postdata(); ?>
            </div>
        </div>
        </div>
    </section>

    <!-- Oportunidade -->
<?php get_template_part( 'components/index/oportunidades' ); ?>

    <!-- Newsletter -->
<?php  get_template_part('components/index/newsletter'); ?>

<?php get_footer(); ?>