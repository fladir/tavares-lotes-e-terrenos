(function ($) {
    $(document).ready(function () {

        $(".floating-contacts").hover(
            function () {
                $(this).addClass('animated tada');
            },
            function () {
                $(this).removeClass('animated tada');
            }
        );

        $('.btn-navbar a').attr('data-toggle', 'modal').attr('data-target', '.bd-example-modal-lg-corretor');

        // Carousel Tratamentos Cirúrgicos
        $(".single-imovel-carousel").owlCarousel({
            navigation: true,
            loop: true,
            paginationSpeed: 400,
            autoplay: true,
            autoplayHoverPause: true,
            smartSpeed: 700,
            autoplayTimeout: 5000,
            responsiveClass: true,
            slideBy: 1,
            items: 3,
            dots: false,
            nav: true,
            margin: 0,
            navText: ['<i class="fas fa-arrow-left"></i>', '<i class="fas fa-arrow-right"></i>'],
            responsive : {
                // breakpoint from 0 up
                0 : {
                    items: 1,
                },
                // breakpoint from 768 up
                768 : {
                    items: 2,
                },
                990 : {
                    items: 3,
                }
            }
        });


        $(".depoimentos-carousel").owlCarousel({
            navigation: true,
            loop: true,
            paginationSpeed: 300,
            autoplay: true,
            autoplayHoverPause: true,
            smartSpeed: 700,
            autoplayTimeout: 5000,
            responsiveClass: true,
            slideBy: 1,
            items: 1,
            dots: true,
            nav: false,
            margin: 30,
            navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>']
        });

        //WOW Effect
        wow = new WOW(
            {
                boxClass: 'wow',      // default
                animateClass: 'animated', // default
                offset: 0,          // default
                mobile: true,       // default
                live: true        // default
            }
        );
        wow.init();
    });

    //Botão Menu Mobile
    $('.nav-toggler, .mobile-menu-overlay').click(function () {
        $('.nav-toggler').toggleClass('toggler-open');
        $('.mobile-menu-wrapper, .mobile-menu-overlay').toggleClass('open');
        $('body, #menu-secundario').toggleClass('arredar');
    });

    //Contagem Nossos Números
    $('#nossos-numeros').waypoint(function () {
        $('.count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 3000,
                easing: 'easeOutExpo',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        $('.numeros').addClass('animated fadeIn faster');
        this.destroy()
    }, {offset: '50%'});

    $('.nav-link').removeAttr('title');

// Botão voltar ao topo
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#voltarTopo').addClass('show')
        } else {
            $('#voltarTopo').removeClass('show');
        }
    });
    $('#voltarTopo').click(function () {
        $("html, body").animate({scrollTop: 0}, 1200, 'easeInOutExpo');
        return false;
    });

    $(".view-products").click(function (event) {
        event.preventDefault();
        $('html,body').animate({
                scrollTop: $("#blocks").offset().top
            },
            1200, 'easeInOutExpo');
    });


    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $('#medafi_5b9811146bf31').select2({
        placeholder: 'Comprar/Alugar',
        width: 'resolve',
        minimumResultsForSearch: -1
    });
    $('.home1_adsrchfrm #medafi_5b4757eaeb76a').select2({
        placeholder: 'Tipo de Imóvel',
        width: 'resolve',
        minimumResultsForSearch: -1
    });
    $('.bairro').select2({
        placeholder: 'Bairro',
        width: 'resolve',
        minimumResultsForSearch: -1
    });
    $('.home1_adsrchfrm .quartos').select2({
        placeholder: 'quartos',
        width: 'resolve',
        minimumResultsForSearch: -1
    });

})(jQuery);

