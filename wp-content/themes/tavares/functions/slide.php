<?php 

#SLIDE
$args = array(
    'supports'  => array('title', 'editor', 'thumbnail'),
    'menu_icon'           => 'dashicons-laptop',
  );
  $custom_post_type_slide = new pbo_register_custom_post_type('slide', 'Slide', $args);
  
  $meta_box_slide = new pbo_register_meta_box('pbo_meta_slide', 'Link do botão', array('slide') );
  
  $args = array(
    'label' => 'Link',
    'atributos' => array(
        'id' => 'linkSlider',
        'placeholder' => 'https://www.example.com.br',
        'name' => 'link',
    )
  );
  
  $meta_box_slide->add_field_form('text', $args);