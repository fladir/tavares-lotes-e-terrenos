<?php
/* ----------------- Função para criar o Custom Post Type Imóvel --------------- */
function criar_imoveis(){
    $labels = array(
        'name' => _x('Imóveis', 'post type general name', 'your_text_domain'),
        'singular_name' => _x('Imóvel', 'post type Singular name', 'your_text_domain'),
        'add_new' => _x('Adicionar Imóvel', 'docente', 'your_text_domain'),
        'add_new_item' => __('Adicionar Novo Imóvel', 'your_text_domain'),
        'edit_item' => __('Editar Imóvel', 'your_text_domain'),
        'new_item' => __('Novo Imóvel', 'your_text_domain'),
        'all_items' => __('Todos Imóveis', 'your_text_domain'),
        'view_item' => __('Visualizar Imóveis', 'your_text_domain'),
        'search_items' => __('Procurar Imóveis', 'your_text_domain'),
        'not_found' => __('Nenhum Imóvel Encontrado', 'your_text_domain'),
        'not_found_in_trash' => __('Não hà Imóveis na Lixeira', 'your_text_domain'),
        'parent_item_colon' => '',
        'menu_name' => __('Imóveis', 'your_text_domain')
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'imovel'),
        'capability_type' => 'page',
        'has_archive' => true,
        'hierarchical' => true,
        'menu_position' => null,
        'menu_icon' => 'dashicons-admin-home',
        'supports' => array('title', 'thumbnail', 'editor','excerpt','page-attributes')
    );
    $labels = array(
        'name' => __('Tipo do Imóvel'),
        'singular_name' => __('Tipo do Imóvel'),
        'search_items' => __('Buscar'),
        'popular_items' => __('Mais usados'),
        'all_items' => __('Todos Tipos do Imóvel'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Add novo'),
        'update_item' => __('Atualizar'),
        'add_new_item' => __('Adicionar novo tipo do imóvel'),
        'new_item_name' => __('Novo')
    );
    register_taxonomy('imoveis', array('imovel'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'singular_label' => 'Tipo do Imóvel',
            'all_items' => 'Tipo do Imóvel',
            'query_var' => true,
            'rewrite' => array('slug' => 'imoveis'))
    );



    $labels = array(
        'name' => __('Bairros'),
        'singular_name' => __('Bairro'),
        'search_items' => __('Buscar'),
        'popular_items' => __('Mais usados'),
        'all_items' => __('Todas os Bairros'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Add novo'),
        'update_item' => __('Atualizar'),
        'add_new_item' => __('Adicionar novo Bairro'),
        'new_item_name' => __('Novo')
    );
    register_taxonomy('bairros', array('imovel'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'singular_label' => 'Categoria',
            'all_items' => 'Categoria',
            'query_var' => true,
            'rewrite' => array('slug' => 'bairro'))
    );
    $labels = array(
        'name' => __('Cidades'),
        'singular_name' => __('Cidade'),
        'search_items' => __('Buscar'),
        'popular_items' => __('Mais usados'),
        'all_items' => __('Todas as Cidades'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Add novo'),
        'update_item' => __('Atualizar'),
        'add_new_item' => __('Adicionar nova Cidade'),
        'new_item_name' => __('Novo')
    );
    register_taxonomy('cidades', array('imovel'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'singular_label' => 'Categoria',
            'all_items' => 'Categoria',
            'query_var' => true,
            'rewrite' => array('slug' => 'cidade'))
    );

    $labels = array(
        'name' => __('Comercialização'),
        'singular_name' => __('Comercialização'),
        'search_items' => __('Buscar'),
        'popular_items' => __('Mais usados'),
        'all_items' => __('Todas as Categorias'),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __('Add novo'),
        'update_item' => __('Atualizar'),
        'add_new_item' => __('Adicionar novo tipo de Comercialização'),
        'new_item_name' => __('Novo')
    );
    register_taxonomy('comercializacao', array('imovel'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'singular_label' => 'Comercialização',
            'all_items' => 'Tipos de Comercilização',
            'query_var' => true,
            'rewrite' => array('slug' => 'comercializacao'))
    );

    register_post_type('imovel', $args);
    flush_rewrite_rules();
}
add_action('init', 'criar_imoveis');
/* ----------------- FIM da  Função para criar o Custom Post Type Imóvel --------------- */

add_action('init', 'type_post_depoimentos');

function type_post_depoimentos()
{
    $labels = array(
        'name' => _x('Depoimentos', 'post type general name'),
        'singular_name' => _x('Depoimento', 'post type singular name'),
        'add_new' => _x('Adicionar Novo Depoimento', 'Novo Depoimento'),
        'add_new_item' => __('Novo Depoimento'),
        'edit_item' => __('Editar Depoimento'),
        'new_item' => __('Novo Depoimento'),
        'view_item' => __('Ver Depoimento'),
        'search_items' => __('Procurar Depoimentos'),
        'not_found' => __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Depoimentos'
    );


    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-testimonial',
        'supports' => array('title', 'editor', 'thumbnail'),
    );

    register_post_type('depoimento', $args);
    flush_rewrite_rules();
}

add_action('init', 'type_post_localidades');

function type_post_localidades()
{
    $labels = array(
        'name' => _x('Localidades', 'post type general name'),
        'singular_name' => _x('Depoimento', 'post type singular name'),
        'add_new' => _x('Adicionar Nova Localidade', 'Nova Localidade'),
        'add_new_item' => __('Nova Localidade'),
        'edit_item' => __('Editar Localidade'),
        'new_item' => __('Nova Localidade'),
        'view_item' => __('Ver Localidade'),
        'search_items' => __('Procurar Localidades'),
        'not_found' => __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Localidades'
    );


    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-location',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
    );



	$labels = array(
		'name' => __('Categoria Localidade'),
		'singular_name' => __('Localidade'),
		'search_items' => __('Buscar'),
		'popular_items' => __('Mais usados'),
		'all_items' => __('Todas as Localidades'),
		'parent_item' => null,
		'parent_item_colon' => null,
		'edit_item' => __('Add novo'),
		'update_item' => __('Atualizar'),
		'add_new_item' => __('Adicionar novo tipo do imóvel'),
		'new_item_name' => __('Novo')
	);
	register_taxonomy('localidade', array('localidade'), array(
			'hierarchical' => true,
			'labels' => $labels,
			'singular_label' => 'Localidade',
			'all_items' => 'Localidade',
			'query_var' => true,
			'rewrite' => array('slug' => 'localidade'))
	);




    register_post_type('localidade', $args);
    flush_rewrite_rules();
}



?>