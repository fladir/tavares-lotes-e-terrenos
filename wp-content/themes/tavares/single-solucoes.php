<?php get_header(); ?>

    <!-- Topo -->
<?php  get_template_part('components/topo-da-pagina/topo-da-pagina'); ?>

    <!-- Conteúdo Solução -->
<?php  get_template_part('components/solucoes/solucao'); ?>

    <!-- O que está incluído -->
<?php  get_template_part('components/solucoes/o-que-esta-incluido'); ?>

    <!-- Call to Action -->
<?php  get_template_part('components/call-to-action/cta'); ?>

    <!-- Vantagens -->
<?php  get_template_part('components/solucoes/vantagens'); ?>

    <!-- Rastreamento -->
<?php  get_template_part('components/rastreamento/rastreamento'); ?>

<?php get_footer(); ?>