<?php
get_header();
?>

    <section id="pagina">
        <div class="container">
			<?php if ( have_posts() ) : while ( have_posts() ) :
			the_post(); ?>
			<?php the_content(); ?>
        </div>
		<?php endwhile;
		endif;
		wp_reset_postdata(); ?>
    </section>


<?php get_footer(); ?>