<?php
get_header();
$grupoTopoDaPaginaGeral = get_field( 'grupo_conteudos_dos_componentes', 'options' )['imagem_de_fundo'];
$cidades                = get_field( 'cidade' );
#echo '<pre>'; print_r($cidades); echo '</pre>';

?>

    <section class="title-and-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
					<?php
					if ( function_exists( 'yoast_breadcrumb' ) ) {
						yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
					}
					?>
                    <h1 class="the_title mb-4">
						<?php the_title() ?>
                    </h1>
                </div>
            </div>
        </div>
    </section>

    <!-- Post -->
    <section id="single-localidade">
        <article>
            <div class="container">
                <div class="row">
                    <div class="col-12 content text-left">
						<?php if ( has_post_thumbnail() ) : ?>
							<?php the_post_thumbnail( 'single_post', array(
								'alt'   => '' . get_the_title() . '',
								'title' => '' . get_the_title() . ''
							) ); ?>
						<?php endif; ?>
                        <div class="mt-5">
							<?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>
    </section>


    <!-- Feature Properties -->
    <section id="feature-property" class="feature-property-home6 oportunidades-home mt-0 pt=0">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-title mb40">
                        <h2>OPORTUNIDADES EM ESMERALDAS</h2>
                    </div>
                </div>
                <div class="row w-100">

					<?php

					$args    = array(
						'cat'            => 'all',
						'paged'          => $paged,
						'post_type'      => 'imovel',
						'orderby'        => 'date',
						'order'          => 'DESC',
						'category_name'  => '',
						'posts_per_page' => 6
					);
					$WPQuery = new WP_Query( $args );
					?>
					<?php
					$destaque = get_field( 'destaque_oportunidades' );
					if ( $WPQuery->have_posts() ) : while ( $WPQuery->have_posts() ) : $WPQuery->the_post();
						$tipoImovel = get_field( 'tipo_de_imovel' );
						$destaque   = get_field( 'destaque' );
						if ( $destaque && $tipoImovel === 'Comprar' ) :
							?>

                            <div class="col-md-4">
                                <div class="properti_city home6">
                                    <div class="thumb">
										<?php the_post_thumbnail( 'oportunidades_home' ); ?>
                                        <div class="thmb_cntnt">
                                            <ul class="tag mb0">
												<?php // Get terms for post
												$terms = get_the_terms( $post->ID, 'imoveis' );
												// Loop over each item since it's an array
												if ( $terms != null ) {
													foreach ( $terms as $term ) {
														// Print the name method from $term which is an OBJECT
														echo '<li class="list-inline-item"><a href="#">';
														print $term->name;
														echo '</a>
                                            </li> ';
														// Get rid of the other data stored in the object, since it's not needed
														unset( $term );
													}
												} ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="overlay">
                                        <div class="details">
                                            <a class="fp_price"
                                               href="#">R$ <?php echo get_field( 'preco' ) ?> <?php echo $tipoImovel == 'Alugar' ? '/mês' : '' ?>
                                                <small></small></a>
                                            <a href="<?php the_permalink(); ?>"><h4><?php the_title() ?></h4></a>
											<?php if ( get_field( 'codigo' ) ) : ?>
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-12" style="left: -14px;"><p>
                                                                Cód. <?php echo get_field( 'codigo' ) ?></p></div>
                                                    </div>
                                                </div>
											<?php endif; ?>


                                        </div>
                                    </div>
                                </div>
                            </div>
						<?php
						endif;
					endwhile; endif;
					wp_reset_postdata(); ?>

                </div>
            </div>
        </div>
    </section>

    <!-- Newsletter -->
<?php get_template_part( 'components/index/newsletter' ); ?>
<?php get_footer(); ?>