<?php get_header(); ?>

    <!-- Blog Single Post -->
    <section class="blog_post_container bgc-f7 pb30">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="breadcrumb_content style2">
                        <?php
                        if (function_exists('yoast_breadcrumb')) {
                            yoast_breadcrumb('<p id="breadcrumbs-item">', '</p>');
                        }
                        ?>
                        <h2 class="breadcrumb_title">Blog</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="main_blog_post_content">
                        <div class="mbp_thumb_post">


                            <?php


                            $categories = get_the_category();
                            $separator = ' ';
                            $output = '';
                            if (!empty($categories)) {
                                foreach ($categories as $category) {
                                    $output .= '<div class="blog_sp_tag"><a href="' . esc_url(get_category_link($category->term_id)) . '" alt="' . esc_attr(sprintf(__('View all posts in %s', 'textdomain'), $category->name)) . '">' . esc_html($category->name) . '</a></div>' . $separator;
                                }
                                echo trim($output, $separator);
                            }


                            ?>


                            <h3 class="blog_sp_title"><?php the_title(); ?></h3>
                            <ul class="blog_sp_post_meta">
                                <li class="list-inline-item">
                                    <a><?php echo get_avatar(get_the_author_meta('ID'), 100); ?></a></li>
                                <li class="list-inline-item">
                                    <a><?php echo get_the_author_meta('display_name', $author_id); ?></a></li>
                                <li class="list-inline-item"><span class="flaticon-calendar"></span></li>
                                <li class="list-inline-item"><a><?php echo get_the_date() ?></a></li>


                            </ul>
                            <div class="thumb">
                                <?php if (has_post_thumbnail()) : ?>
                                    <?php the_post_thumbnail('single_post', array('alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                <?php endif; ?>                            </div>
                            <div class="details">
                                <?php the_content(); ?>
                            </div>
                            <ul class="blog_post_share">
                                <li><p>Compartilhar</p></li>
                                <li><a target="_blank"
                                       href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_the_permalink() ?>"><i
                                                class="fab fa-facebook-f"></i></a></li>
                                <li><a target="_blank"
                                       href="https://twitter.com/home?status=<?php echo get_the_permalink() ?>"><i
                                                class="fab fa-twitter"></i></a></li>
                                <li><a target="_blank"
                                       href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_the_permalink() ?>&title=&summary=&source="><i
                                                class="fab fa-linkedin"></i></a></li>
                                <li><a target="_blank"
                                       href="https://api.whatsapp.com/send?phone=&text=<?php echo get_the_permalink() ?>"><i
                                                class="fab fa-whatsapp"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 mb20 mt-5">
                            <h3>Posts Relacionados</h3>
                        </div>
                        <?php

                        $related = get_posts(array('category__in' => wp_get_post_categories($post->ID), 'numberposts' => 5, 'post__not_in' => array($post->ID)));
                        if ($related) foreach ($related as $post) {
                            setup_postdata($post); ?>
                            <div class="col-md-6 col-lg-6">
                                <div class="for_blog feat_property">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="thumb">
                                            <?php the_post_thumbnail('img_post_list', array('class' => 'img-blog', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                            <div class="tag">
                                                <?php
                                                $post_id = get_the_ID(); // or use the post id if you already have it
                                                $category_object = get_the_category($post_id);
                                                $category_name = $category_object[0]->name;
                                                echo $category_name;
                                                ?>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="details">
                                        <div class="tc_content">
                                            <a href="<?php the_permalink(); ?>">
                                                <h4><?php the_title() ?></h4>
                                            </a>
                                            <ul class="bpg_meta">
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="flaticon-calendar"></i></a></li>
                                                <li class="list-inline-item"><a
                                                            href="#"><?php echo get_the_date() ?></a></li>
                                            </ul>
                                            <p><?php the_excerpt(); ?></p>
                                        </div>
                                        <div class="fp_footer">
                                            <ul class="fp_meta float-left mb0">
                                                <li class="list-inline-item"><a
                                                            href="#"><?php echo get_avatar(get_the_author_meta('ID'), 100); ?></a>
                                                </li>
                                                <li class="list-inline-item"><a
                                                            href="#"><?php echo get_the_author_meta('display_name', $author_id); ?></a>
                                                </li>
                                            </ul>
                                            <a class="fp_pdate float-right text-thm" href="<?php the_permalink(); ?>">Leia
                                                Mais <span
                                                        class="flaticon-next"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php }
                        wp_reset_postdata(); ?>
                    </div>
                </div>
                <div class="col-lg-4">
                    <?php dynamic_sidebar('blog_sidebar'); ?>
                </div>
            </div>
        </div>
    </section>


<?php get_footer(); ?>