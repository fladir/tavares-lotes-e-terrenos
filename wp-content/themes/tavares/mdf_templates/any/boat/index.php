<?php if (!defined('ABSPATH')) die('No direct access allowed'); ?>
<?php
global $mdf_loop;
MDTF_SORT_PANEL::mdtf_catalog_ordering();
?>
<div class="clearfix"></div>
<?php
$i = 1;

$boots_types = MetaDataFilterCore::get_html_items(1005);

while ($mdf_loop->have_posts()) : $mdf_loop->the_post();
    ?>
    <article class="list_grid">
        <figure>             
            <a href="<?php the_permalink(); ?>"><?php wpsm_thumb('grid_news') ?></a>
        </figure>
        <div class="content_constructor">
            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <div class="rehub_catalog_desc">                                 
                <?php kama_excerpt('maxchar=150'); ?>                       
            </div>                        


            <div class="CSSTableGenerator" >
                <table >
                    <tr>



                    </tr>
                    <tr>
                        <td >
                            <div class="rehub_catalog_field_title"><span><?php _e('Preis: CHF ', 'rehub_framework'); ?></span></div>
                            <div class="rehub_catalog_field_value"><?php echo get_post_meta($post->ID, "medafi_54c52851e1ca3", true); ?>,-</div>
                        </td>
                        <td>
                            <div class="rehub_catalog_field_title"><span><?php _e('Typ:', 'rehub_framework'); ?></span></div>
                            <div class="rehub_catalog_field_value"><?php echo get_post_meta($post->ID, "medafi_548f2c775170c", true); ?></div>
                        </td>

                    </tr>
                    <tr>
                        <td >
                            <div class="rehub_catalog_field_title"><span><?php _e('Jahrgang:', 'rehub_framework'); ?></span></div>
                            <div class="rehub_catalog_field_value"><?php echo get_post_meta($post->ID, "medafi_548b405a1bace", true); ?></div>
                        </td>
                        <td>
                            <div class="rehub_catalog_field_title"><span><?php _e('Breite:', 'rehub_framework'); ?></span></div>
                            <div class="rehub_catalog_field_value"><?php echo get_post_meta($post->ID, "medafi_548c46663da82", true); ?> m</div>
                        </td>

                    </tr>
                    <tr>
                        <td >
                            <div class="rehub_catalog_field_title"><span><?php _e('Angebot:', 'rehub_framework'); ?></span></div>
                            <div class="rehub_catalog_field_value"><?php echo get_post_meta($post->ID, "medafi_548dd9dba7dec", true); ?></div>
                        </td>
                        <td>
                            <div class="rehub_catalog_field_title"><span><?php _e('Länge:', 'rehub_framework'); ?></span></div>
                            <div class="rehub_catalog_field_value"><?php echo get_post_meta($post->ID, "medafi_548c4692d6f98", true); ?> m</div>
                        </td>

                    </tr>
                    <tr>
                        <td >
                            <div class="rehub_catalog_field_title"><span><?php _e('Typ 2:', 'rehub_framework'); ?></span></div>
                            <div class="rehub_catalog_field_value"><?php
                                $res = array();
                                if (!empty($boots_types) AND is_array($boots_types)) {
                                    foreach ($boots_types as $key => $dt) {
                                        if ($dt['type'] !== 'checkbox') {
                                            continue;
                                        }
                                        //+++
                                        $ch = get_post_meta($post->ID, $key, true);
                                        if ($ch == 1) {
                                            $res[] = $dt['name'];
                                        }
                                    }
                                }
                                $res = implode(', ', $res);
                                echo $res;
                                /*
                                  $ch = get_post_meta($post->ID, "medafi_54c50fac8060f", true);
                                  if ($ch == 1) {
                                  //checkbox is checked
                                  }
                                 */
                                ?></div>
                        </td>
                        <td>

                        </td>

                    </tr>

                </table>
            </div>





        </div>                       
    </article>
    <?php
    $i++;
endwhile; // end of the loop.     
?> 
<div class="clearfix"></div>