<?php
$galeria = get_field('galeria_biografia');
?>


<section id="galeria-biografia" class="mb-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3 class="text-center text-secundario text-uppercase fw-semi-bold mb-5">
                    <?php echo get_field('titulo_da_galeria') ?>
                </h3>
            </div>
            <?php if ($galeria) : foreach ($galeria as $item) : ?>
                <div class="col-6 col-md-3 mb-4">
                    <a data-fancybox="gallery" class="position-relative d-block" href="<?php print_r($item['sizes']['full_galeria']) ?>">
                        <img src="<?php print_r($item['sizes']['thumb_galeria']) ?>">
                        <span class="overlay d-flex justify-content-center align-items-center"><i class="fas fa-search-plus"></i></span>
                    </a>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </div>
</section>
