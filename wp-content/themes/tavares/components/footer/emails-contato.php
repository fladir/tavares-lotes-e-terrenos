<div class="row mb-40">
    <?php
    $emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
    foreach ($emails as $email) :
    ?>
        <div class="col-12 col-md-6 col-lg-4 my-3 my-md-1">
            <h5><?php echo $email['setor']; ?></h5>
            <a href="mailto:<?php echo $email['endereco_email']; ?>"><?php echo $email['endereco_email']; ?></a>
        </div>
    <?php endforeach; ?>
</div>