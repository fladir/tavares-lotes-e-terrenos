<?php
$botaoFormulario = get_field('texto_botao_formulario_contato')
?>
<div class="row">
    <div class="col-12">
        <form id="formularioContato" class="form-signin p-0 m-0" method="post" action="<?php bloginfo('url') ?>/resposta/" id="smart-form" enctype="multipart/form-data">
            <input type="hidden" name="formulario" value="<?php echo the_title(); ?>">
            <?php /* INICIO: NÃO REMOVER OU ALTERAR OS CAMPOS \/ */?>
            <div class="section" style="display:none">
                <label class="field prepend-icon">
                    <input type="text" name="formulario" id="formulario" value="<?php echo str_replace('/' , '', basename(get_permalink())); ?>" class="gui-input nome">
                    <b><i class="fa fa-cogs" aria-hidden="true"></i> Página do formulário</b>
                </label>
            </div>
            <div class="section" style="display:none">
                <label class="field prepend-icon">
                    <input type="text" name="baseurl" id="baseurl" value="<?php echo get_site_url(); ?>" class="gui-input nome">
                    <b><i class="fa fa-cogs" aria-hidden="true"></i> BASE URL</b>
                </label>
            </div>
            <?php /* FINAL: NÃO REMOVER OU ALTERAR OS CAMPOS /\ */?>
            <div class="form-label-group mb-3">
                <input type="nome" id="nomeContato" class="form-control" placeholder="Nome Completo" name="nome"
                       required autofocus>
            </div>

            <div class="form-label-group mb-3">
                <input type="text" id="telefoneContato" class="form-control" placeholder="Telefone" name="telefone"
                       required autofocus>
            </div>

            <div class="form-label-group mb-3">
                <input type="email" id="emailContato" class="form-control" placeholder="Email" name="email" required
                       autofocus>
            </div>

            <div class="form-label-group mb-3">
                <textarea class="form-control" id="mensagemContato" placeholder="Sua mensagem" name="mensagem" required
                          autofocus rows="4"></textarea>
            </div>

            <div class="form-label-group">
                <div class="row">
                    <div class="col-6 pr-2">
                        <input type="text" class="form-control lgui-input sfcode" placeholder="Digite o Código"
                               name="securitycode" id="securitycode" required autofocus>
                    </div>
                    <div class="col-6 pl-2">
                        <div class="button captcode">
                            <img src="<?php bloginfo('template_url') ?>/components/formularios/captcha/captcha.php"
                                 id="captcha"
                                 alt="Captcha"/>
                            <span class="refresh-captcha"><i class="fas fa-sync-alt"></i></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="button_su mt-4 w-100">
                <span class="su_button_circle"></span>
                <button id="btnSubmit" type="submit" class="btn button_su_inner primario w-100">
                    <span class="button_text_container"><?php echo $botaoFormulario; ?></span>
                </button>
            </div>
        </form>
    </div>
</div>
<?php wp_enqueue_script('jquery-validate', get_bloginfo('template_url') . '/node_modules/jquery-validation/dist/jquery.validate.min.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery-validate-translate', get_bloginfo('template_url') . '/assets/js/messages_pt_BR.min.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery-maskedinput', get_bloginfo('template_url') . '/node_modules/jquery.maskedinput/src/jquery.maskedinput.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery_form', get_bloginfo('template_url') . '/assets/js/captcha/jquery.form.min.js', array('jquery')) ?>
<?php wp_enqueue_script('smart_form', get_bloginfo('template_url') . '/assets/js/captcha/smart-form.js', array('jquery')) ?>