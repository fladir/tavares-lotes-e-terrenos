<div class="smart-wrap">
    <div class="smart-forms smart-container wrap-2">
        <div class="form-header header-primary"></div><!-- end .form-header section -->
        <div class="form-contact-wrap m-t-40">

            <form class="xs-form" method="post" action="<?php bloginfo('url') ?>/resposta/" id="smart-form" enctype="multipart/form-data">
                <input type="hidden" name="formulario" value="<?php echo the_title(); ?>">
                <div class="form-body">
                    <?php /* INICIO: NÃO REMOVER OU ALTERAR OS CAMPOS \/ */?>
                    <div class="section" style="display:none">
                        <label class="field prepend-icon">
                            <input type="text" name="formulario" id="formulario" value="<?php echo str_replace('/' , '', basename(get_permalink())); ?>" class="gui-input nome">
                            <b><i class="fa fa-cogs" aria-hidden="true"></i> Página do formulário</b>
                        </label>
                    </div>
                    <div class="section" style="display:none">
                        <label class="field prepend-icon">
                            <input type="text" name="baseurl" id="baseurl" value="<?php echo get_site_url(); ?>" class="gui-input nome">
                            <b><i class="fa fa-cogs" aria-hidden="true"></i> BASE URL</b>
                        </label>
                    </div>
                    <?php /* FINAL: NÃO REMOVER OU ALTERAR OS CAMPOS /\ */?>
                    <div class="row">
                        <div class="col-12">
                            <input required type="text" name="nome" id="nome" class="form-control mb-3" placeholder="Nome" >
                        </div>
                        <div class="col-12">
                            <input required type="text" name="tel" id="tel" class="form-control mb-3" placeholder="Telefone">
                        </div>
                        <div class="col-12">
                            <input required type="email" name="email" id="email" class="form-control invaild mb-3" placeholder="E-mail">
                        </div>
                    </div>

                    <div class="row">
                           <div class="col-12">
                            <textarea required class="form-control message-box mb-3" id="mensagem" name="mensagem" cols="30" rows="4" placeholder="Sua Mensagem"></textarea>
                        </div>
                    </div>

                    <div class="button-captcha">
                        <div class="section cap">
                            <div class="captcha">
                                <div class="row ">
                                    <div class="col-md-6 pr-2">
                                        <input required type="text" name="securitycode" id="securitycode" class="gui-input sfcode form-control" placeholder="Digite o Código">
                                    </div>
                                    <div class="col-md-6 pl-2">
                                        <div class="button captcode">
                                            <img src="<?php bloginfo('template_url') ?>/components/formularios/captcha/captcha.php" id="captcha" alt="Captcha"/>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex justify-content-end">

                                        <div class="button_su mt-4 w-100">
                                            <span class="su_button_circle"></span>
                                            <button name="LetheForm_Save" id="LetheForm_Save" type="submit" class="btn button_su_inner primario w-100">
                                                <span class="button_text_container">Enviar</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="result"></div><!-- end .result  section -->
                </div>
                <div class="msgs-formulario">
                </div>
            </form>

        </div>
    </div>
</div>
<?php wp_enqueue_script('jquery-validate', get_bloginfo('template_url') . '/node_modules/jquery-validation/dist/jquery.validate.min.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery-validate-translate', get_bloginfo('template_url') . '/assets/js/messages_pt_BR.min.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery-maskedinput', get_bloginfo('template_url') . '/node_modules/jquery.maskedinput/src/jquery.maskedinput.js', array('jquery')) ?>
<?php wp_enqueue_script('jquery_form', get_bloginfo('template_url') . '/assets/js/captcha/jquery.form.min.js', array('jquery')) ?>
<?php wp_enqueue_script('smart_form', get_bloginfo('template_url') . '/assets/js/captcha/smart-form.js', array('jquery')) ?>
<?php wp_enqueue_script('sweetAlert', get_bloginfo('template_url') . '/node_modules/sweetalert/dist/sweetalert.min.js',array('jquery'))?>
