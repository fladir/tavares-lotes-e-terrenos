<section id="newsletter">

    <div class="container">

        <div class="row">
            <div class="col-md-4">
                <h3 class=" text-white text-md-left text-center mb-3">
                    Inscreva-se em nossa News Letter
                </h3>
                <p class="mb-3 text-white text-md-left text-center">
                    E fique por dentro das novidades da LogLife
                </p>
            </div>
            <div class="col-md-12">
                <img class="img-newsletter"
                     src="<?php echo get_template_directory_uri() . '/assets/img/fundo-newsletter.png'; ?>"
                     alt="">

                <?php echo do_shortcode('[contact-form-7 id="371" title="Newsletter"]'); ?>

            </div>
        </div>



    </div>

</section>

<?php /* Restore original Post Data */
wp_reset_postdata(); ?>