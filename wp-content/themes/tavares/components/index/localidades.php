<?php
$args    = array(
	'cat'            => 'all',
	'paged'          => $paged,
	'post_type'      => 'localidade',
	'orderby'        => 'date',
	'order'          => 'DESC',
	'category_name'  => '',
	'posts_per_page' => 4
);
$WPQuery = new WP_Query( $args );
?>

<section class="localidades-home">
    <div class="container">
        <div class="row">
            <div class="col-12 p-0 mb-4">
                <h3 class="text-uppercase">Onde Estão Nossos Imóveis</h3>
            </div>
        </div>
        <div class="row">
			<?php if ( $WPQuery->have_posts() ) : while ( $WPQuery->have_posts() ) : $WPQuery->the_post(); ?>
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ), 'localidades_home' ); ?>
                <div class="content-localidade-home mb-4" style="background-image: url(<?php echo $url ?>)">
                    <div class="overlay-filter"></div>
                    <div class="conteudo-localidade py-4 px-5">
                        <h3 class="text-white">
							<?php the_title() ?>
                        </h3>
						<?php the_excerpt(); ?>
                        <a class="button btn-primario" href="<?php the_permalink(); ?>">
                            Leia Mais
                        </a>
                    </div>
                </div>

			<?php

			endwhile; endif;
			wp_reset_postdata(); ?>
        </div>
    </div>
</section>
