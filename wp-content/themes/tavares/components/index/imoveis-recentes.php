<!-- Feature Properties -->
<section id="feature-property" class="feature-property-home6">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-title mb40">
                    <h2>IMÓVEIS RECENTES</h2>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="row w-100">
                    <?php

                    $args = array(
                        'cat' => 'all',
                        'paged' => $paged,
                        'post_type' => 'imovel',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'category_name' => '',
                        'posts_per_page' => 4
                    );
                    $WPQuery = new WP_Query($args);
                    ?>
                    <?php
                    $destaque = get_field('destaque');
                    if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post();
                        $tipoImovel = get_field('tipo_de_imovel');
                        $destaque = get_field('destaque');
                        if ($destaque && $tipoImovel === 'Comprar') :
                            $terms = get_terms(
                                array(
                                    'taxonomy' => 'comercializacao',
                                    'hide_empty' => false,
                                )
                            );

                            ?>

                            <div class="col-md-3">

                                <div class="properti_city home6">

                                        <div class="thumb">

                                            <a href="<?php the_permalink(); ?>">
                                            <?php the_post_thumbnail('imoveis_home'); ?>
                                            </a>
                                            <div class="thmb_cntnt">
                                                <ul class="tag mb0">
	                                                <?php // Get terms for post
	                                                $terms = get_the_terms( $post->ID, 'imoveis' );
	                                                // Loop over each item since it's an array
	                                                if ( $terms != null ) {
		                                                foreach ( $terms as $term ) {
			                                                // Print the name method from $term which is an OBJECT
			                                                echo '<li class="list-inline-item"><a href="#">';
			                                                print $term->name;
			                                                echo '</a>
                                            </li> ';
			                                                // Get rid of the other data stored in the object, since it's not needed
			                                                unset( $term );
		                                                }
	                                                } ?>
                                                </ul>
                                            </div>

                                        </div>


                                    <div class="overlay">
                                        <div class="details">
                                            <a class="fp_price" href="#">R$ <?php echo get_field('preco') ?></a>
                                            <a href="<?php the_permalink(); ?>"><h4><?php the_title() ?></h4></a>
	                                        <?php if ( get_field( 'codigo' ) ) : ?>
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-12" style="left: -14px;"><p>
                                                                Cód. <?php echo get_field( 'codigo' ) ?></p></div>
                                                    </div>
                                                </div>
	                                        <?php endif; ?>

                                        </div>
                                    </div>

                                </div>


                            </div>
                        <?php
                        endif;
                    endwhile; endif;
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
