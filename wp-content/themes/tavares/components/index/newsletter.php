<?php

?>

<section class="newsletter-home">
    <div class="container">
        <div class="row">
            <div class="col-12">
				<?php echo get_field( 'titulo_newsletter', 135 ) ?>
            </div>
            <div class="col-md-8 p-0">
				<?php echo do_shortcode( '[get_news_letter_mail mail="Email" buttom="Enviar"]' ) ?>
            </div>
        </div>
    </div>
    <img src="<?php print_r( get_field( 'imagem_newsletter', 135 )['sizes']['img_newsletter_home'] ) ?>" alt="CADASTRE-SE EM NOSSA LISTA VIP
e receba dicas e Oportunidades únicas de imóveis." title="CADASTRE-SE EM NOSSA LISTA VIP
e receba dicas e Oportunidades únicas de imóveis." class="img-newsletter-home">
</section>
