<pre>
    <?php #print_r( get_field( 'quem_somos' )['imagem']['sizes']['fundo_secao'] ) ?>
</pre>

<section class="quem-somos"
         style="background-image: url(<?php print_r( get_field( 'quem_somos' )['imagem_de_fundo']['sizes']['fundo_secao'] ) ?>)">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-6 pl-5">
                <h3 class="mb-5 mt-4 text-white text-uppercase"><?php echo get_field( 'quem_somos' )['titulo'] ?></h3>
                <p class="text-white fw-semi-bold">
	            <?php echo get_field( 'quem_somos' )['texto'] ?>
                </p>
            </div>
        </div>
    </div>
</section>