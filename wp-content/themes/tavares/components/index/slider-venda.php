<!-- Feature Properties -->
<section id="feature-property" class="feature-property-home6">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-title mb40">
                    <h2>OPORTUNIDADES</h2>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="feature_property_home6_slider">
                    <?php

                    $args = array(
                        'cat' => 'all',
                        'paged' => $paged,
                        'post_type' => 'imovel',
                        'orderby' => 'date',
                        'order' => 'DESC',
                        'category_name' => '',
                        'posts_per_page' => -1
                    );
                    $WPQuery = new WP_Query($args);
                    ?>
                    <?php
                    $destaque = get_field('destaque');
                    if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post();
                        $tipoImovel = get_field('tipo_de_imovel');
                        $destaque = get_field('destaque');
                        if ($destaque && $tipoImovel === 'Comprar') :
                            $terms = get_terms(
                                array(
                                    'taxonomy' => 'comercializacao',
                                    'hide_empty' => false,
                                )
                            );

//                            if ( ! empty( $terms ) && is_array( $terms ) ) {
//                                // Run a loop and print them all
//                                foreach ( $terms as $term ) {
                            ?>
                            <!--                                <a href="--><?php //echo esc_url( get_term_link( $term ) )
                            ?><!--">-->
                            <!--                                    --><?php //echo $term->name;
                            ?>
                            <!--                                    </a>--><?php
//                                }
//                            }
                            ?>

                            <div class="item">

                                <div class="properti_city home6">

                                        <div class="thumb">
                                            <a href="<?php the_permalink(); ?>">
                                            <?php the_post_thumbnail('imoveis_home'); ?>
                                            </a>
                                            <div class="thmb_cntnt">
                                                <ul class="tag mb0">
                                                    <li class="list-inline-item"><a
                                                                href="#"><?php echo $tipoImovel ?></a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>


                                    <div class="overlay">
                                        <div class="details">
                                            <a class="fp_price" href="#">R$ <?php echo get_field('preco') ?>
                                                <small></small></a>
                                            <a href="<?php the_permalink(); ?>"><h4><?php the_title() ?></h4></a>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-12" style="left: -14px;"><p>
                                                            Cód. <?php echo get_field('codigo') ?></p></div>
                                                    <div title="Área" style="left: 7px;"
                                                         class="col-md-4 d-flex justify-content-center align-content-center">
                                                        <img
                                                                src="<?php bloginfo('template_directory'); ?>/assets/img/area.svg"
                                                                class="mr-2" style="width:18px !important;"/>
                                                        <p><?php echo get_field('area') ?>m²</p></div>
                                                    <br>
                                                    <div title="Quartos" style="left: 14px;"
                                                         class="col-md-4 d-flex justify-content-center align-content-center">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/quartos.svg"
                                                             class="mr-2" style="width:18px !important;"/>
                                                        <p><?php echo get_field('quartos') ?></p>
                                                    </div>
                                                    <div title="Banheiros" style="left: 7px;"
                                                         class="col-md-4 d-flex justify-content-center align-content-center">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/banheiros.svg"
                                                             class="mr-2" style="width:18px !important;"/>
                                                        <p><?php echo get_field('banheiros') ?></p>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>


                            </div>
                        <?php
                        endif;
                    endwhile; endif;
                    wp_reset_postdata(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
