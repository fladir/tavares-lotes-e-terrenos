<?php
$items = get_field('itens_o_que_vc_procura');
?>

<!-- Property Cities -->
<section id="property-city" class="property-city pb30">

    <img src="<?php print_r( get_field( 'imagem_de_fundo_o_q_vc_deseja' )['sizes']['banner'] ) ?>" alt="<?php the_title() ?>" class="bg-o-q-vc-procura">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="main-title text-center mb30">
                    <h2 class="text-uppercase"><?php echo get_field('titulo_o_que_vc_procura') ?></h2>
                </div>
            </div>
        </div>
        <div class="row features_row">
            <?php if ($items) : foreach ($items as $item) : ?>
                <div class="col-md p0">
                    <div class="why_chose_us home6">
                        <a href="<?php echo $item['link'] ?>">
                            <div class="icon">
                                <img src="<?php print_r($item['icone']['sizes']['icone_o_que_procura']) ?>" alt="">
                                <!--                            <span class="flaticon-house-1"></span>-->
                            </div>

                        <div class="details">
                            <h4><?php echo $item['texto'] ?></h4>
                        </div>
                        </a>
                    </div>
                </div>
            <?php endforeach; endif; ?>
        </div>
    </div>
</section>
