<?php
$args = array(
	'nopaging'       => false,
	'paged'          => $paged,
	'post_type'      => 'post',
	'posts_per_page' => 10,
	'orderby'        => 'post_date',
	'order'          => 'DESC'
);

$WPQuery = new WP_Query( $args );
?>

<section class="blog-home my-4">
    <div class="container">
        <div class="row">
            <div class="col-12 mb-4">
                <h3 class="text-white">
                    BLOG
                </h3>
            </div>
			<?php if ( $WPQuery->have_posts() ) : while ( $WPQuery->have_posts() ) : $WPQuery->the_post(); ?>

                <div class="col-md-4">
                    <div class="card">
                        <a href="<?php the_permalink(); ?>" class="link-img-home">
							<?php the_post_thumbnail( 'img_post_list', array( 'alt'   => '' . get_the_title() . '', 'title' => '' . get_the_title() . '' ) ); ?>
                        </a>
                        <div class="card-body">
	                        <a href="<?php the_permalink(); ?>" class="link-title-home">
                            <h5 class="card-title"><?php the_title() ?></h5>
	                        </a>

							<?php the_excerpt(); ?>

                            <a href="<?php the_permalink(); ?>" class="button btn-primario mt-3">Leia Mais</a>
                        </div>
                    </div>
                </div>

			<?php
			endwhile;
			endif;
			?>
        </div>
    </div>
</section>
