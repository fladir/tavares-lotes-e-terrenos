<?php $banners = get_field( 'conteudo_do_banner' );
#echo '<pre>'; print_r($banners); echo '</pre>'
?>

<section class="home-three">


    <div class="hero-slider-home owl-carousel owl-theme" style="opacity: 1">

		<?php foreach ( $banners as $banner ) : ?>
            <div class="item">
                <img src="<?php print_r( $banner['imagem_do_banner']['sizes']['banner'] ) ?>" alt=""
                     class="img-slider-home">
                <div class="container">
                    <div class="row row-banner">

                        <div class="col-lg-6">
                            <div class="home3_home_content">
                                <h1 class="text-uppercase"><?php echo $banner['titulo'] ?></h1>
                                <h3 class="text-white"><?php echo $banner['subtitulo'] ?></h3>
                            </div>
                        </div>
                        <div class="col-lg-6">

                        </div>
                    </div>
                </div>
            </div>
		<?php endforeach; ?>
    </div>

    <div class="container busca-home">
        <div class="row">
            <div class="col-lg-12">
                <div class="home_adv_srch_opt home3">
                    <div class="tab-content home1_adsrchfrm" id="pills-tabContent">
						<?php echo do_shortcode( '[mdf_search_form id="289"]' ); ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

</section>
