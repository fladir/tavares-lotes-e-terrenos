<?php
//$grupoVantagens = get_field('vantagens');
$vantagens = get_field('vantagens');
?>
<?php //echo '<pre>';
//print_r($vantagens);
//echo '</pre>'; ?>

<section id="vantagens">
    <?php
    if( have_rows('vantagens') ):
    $i = 0;
    while ( have_rows('vantagens') ) : the_row();
    $class = $i % 2 ? 'impar' : 'par';
    ?>
        <?php
        $imagem = get_sub_field('imagem');
        $titulo = get_sub_field('titulo');
        $texto = get_sub_field('texto');
        ?>
    <div class="row <?php echo $class;?> my-5 py-5">
        <div class="col-md-6">
            <img src="<?php echo wp_get_attachment_image_url($imagem, 'vantagens'); ?>"
                 alt="" class="mb-4">
        </div>

        <div class="col-md-6 vantagens-content">
            <h2 class="titulo-destaque duplicate"
                title="<?php echo $titulo; ?>"><?php echo $titulo; ?></h2>
            <?php echo $texto; ?>
        </div>
    </div>
        <?php
        $i++;
    endwhile;
    endif;
    ?>
</section>

