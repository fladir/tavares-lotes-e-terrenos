<?php
$grupoSolucao = get_field('grupo_solucao');
?>

<section id="solucao">
    <div class="row">


        <div class="col-md-6 solucao-content">
            <?php the_post_thumbnail('icone_certificado', array('class' => 'img-archive-portfolio', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
            <h2 class="titulo-destaque duplicate text-center text-md-left mb-4"
                title="Conheça o <?php the_title(); ?>">Conheça o <?php echo the_title(); ?></h2>
            <div class="text-md-left">
                <?php the_content(); ?>
            </div>
        </div>
        <div class="col-md-6">
            <img class="img-solucao" src="<?php echo wp_get_attachment_image_url($grupoSolucao['imagem'], 'vantagens'); ?>"
                 alt="" class="mb-4">
        </div>
    </div>
</section>

