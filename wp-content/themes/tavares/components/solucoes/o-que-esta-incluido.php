<?php
$grupoOQueEstaIncluido = get_field('grupo_o_que_esta_incluido');
$itens = $grupoOQueEstaIncluido['itens']
?>
<?php //echo '<pre>';
//print_r($itens);
//echo '</pre>'; ?>
<section id="o-que-esta-incluido">
    <img class="fundo-o-que-esta-incluido"
         src="<?php echo get_template_directory_uri() . '/assets/img/fundo-coracao.png'; ?>" alt="">

    <div class="container">
        <div class="row mb-5">
            <div class="col-md-12 o-que-esta-incluido-content">
                <h2 class="titulo-destaque duplicate text-left text-md-left mb-4"
                    title="<?php echo $grupoOQueEstaIncluido['titulo'] ?>"><?php echo $grupoOQueEstaIncluido['titulo'] ?></h2>
            </div>
        </div>
        <?php foreach ($itens as $item) : ?>
            <div class="row mb-4 conteudo-o-que-esta-incluido">
                <div class="col-md-1 mb-3">
                    <img class="check-solucoes"
                         src="<?php echo get_template_directory_uri() . '/assets/img/check-solucao.png'; ?>" alt="">
                </div>
                <div class="col-md-11 d-flex align-items-center">
                    <p>
                        <?php echo $item['item'] ?>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>

