<?php

?>

<section class="simular-financiamento mt-5">
	<div class="container">
		<div class="row">
			<img src="<?php bloginfo( 'template_directory' ); ?>/assets/img/fundo-simulacao.jpg" alt="">
			<div class="col-md-6 offset-md-6 pr-5">
				<h3 class="text-white text-center px-5 mb-3">
					Faça uma Simulação de Financiamento
				</h3>
				<?php echo do_shortcode( '[contact-form-7 title="Simulação de Financiamento"]' ); ?>
			</div>
		</div>
	</div>
</section>
