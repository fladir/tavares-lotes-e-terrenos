<?php get_header() ?>

    <!-- Banner -->
<?php  get_template_part('components/banner/banner-and-search'); ?>

    <!-- Oportunidades -->
<?php  get_template_part('components/index/oportunidades'); ?>

    <!-- O que está procurando -->
<?php  get_template_part('components/index/o-que-voce-esta-procurando'); ?>

    <!-- Imóveis Recentes -->
<?php  get_template_part('components/index/imoveis-recentes'); ?>

    <!-- Quem Somos -->
<?php  get_template_part('components/index/quem-somos'); ?>

    <!-- Localidades -->
<?php  get_template_part('components/index/localidades'); ?>

    <!-- Blog -->
<?php  get_template_part('components/index/blog'); ?>

    <!-- Newsletter -->
<?php  get_template_part('components/index/newsletter'); ?>


<?php get_footer() ?>