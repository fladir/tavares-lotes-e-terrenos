<?php
$grupoFooter = get_fields('options')['grupo_footer'];
$telefones = get_field('grupo_informacoes_para_contato', 'options')['telefones'];
$emails = get_field('grupo_informacoes_para_contato', 'options')['emails'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];
$whatsapps = get_field('grupo_informacoes_para_contato', 'options')['whatsapp'];
$redesSociais = get_field('grupo_informacoes_para_contato', 'options')['redes_sociais'];
$enderecos = get_field('grupo_informacoes_para_contato', 'options')['enderecos'];

?>
</div>


<!-- Our Footer -->
<section class="footer_one home5">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4 pr0 pl0">
                <div class="footer_about_widget home5">
                    <a class="logo-footer mb-3 d-block" href="<?php bloginfo('url'); ?>">
                        <?php echo wp_get_attachment_image(get_field('grupo_footer', 'options')['logo_footer'], 'logo'); ?>
                    </a>
                    <p><?php echo $grupoFooter['Texto_footer'] ?></p>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                <div class="footer_qlink_widget home5">
                    <h4>NAVEGUE</h4>

                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'secondary',
                        'depth' => 3,
                        'container' => 'div',
                        'container_class' => 'list-unstyled',
                        'container_id' => 'navbarNav',
                        'menu_class' => 'nav navbar-nav w-100 justify-content-center',
                        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                        'walker' => new WP_Bootstrap_Navwalker(),
                    ));
                    ?>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4 col-xl-4">
                <div class="footer_contact_widget home5">
                    <h4>Contato</h4>
                    <ul class="list-unstyled">
                        <?php foreach ($emails as $email) : ?>
                            <li><a href="mailto:<?php echo $email['endereco_email']; ?>"
                                   target="_blank"><?php echo $email['endereco_email']; ?></a></li>
                        <?php endforeach; ?>

                        <?php foreach ($telefones as $telefone) : ?>
                            <li><a href="tel:<?php echo $telefone['numero_telefone']; ?>"
                                   target="_blank"><?php echo $telefone['numero_telefone']; ?></a></li>
                        <?php endforeach; ?>

                        <?php foreach ($enderecos as $endereco) : ?>
                            <li><a><?php echo $endereco['endereco']; ?></a></li>
                        <?php endforeach; ?>
                        <span id="redes-sociais" class="mt-3 d-block">
                        <?php foreach ($redesSociais as $redeSocial) : ?>
                            <a href="<?php echo $redeSocial['link_social'] ?>" target="_blank"
                               class="rede-social <?php echo $redeSocial['nome_rede_social'] ?>"
                               title="<?php echo $redeSocial['nome_rede_social'] ?>">
                            <i class="<?php echo $redeSocial['icone_social'] ?>"></i>
                        </a>
                        <?php endforeach; ?>
                </span>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Our Footer Bottom Area -->
<section class="footer_middle_area home5 pt30 pb30">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xl-6">

                <div class="copyright-widget home5 text-left">
                    <p>© <?php echo date('Y'); ?> Todos os direitos reservados. <?php echo get_bloginfo('name'); ?></p>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6">
                <div class="footer_menu_widget home5 text-right">
                    <a href="https://bit.ly/2BIb1SZ" target="_blank"> <img
                                src="<?php bloginfo('template_directory'); ?>/assets/img/logo-cia-branca.png"
                                class="logo-cia"/></a>
                </div>
            </div>
        </div>
    </div>
</section>

<?php  get_template_part('components/contatos-flutuantes/contatos-flutuantes'); ?>
<a class="scrollToHome" href="#"><i class="flaticon-arrows"></i></a>

<!-- Modal Simulação -->
<div class="sign_up_modal modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="background-image: url(<?php bloginfo('template_directory'); ?>/assets/img/bg-contato.jpg)">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body container pb20">
                <div class="tab-content container" id="myTabContent">
                    <div class="row mt25 tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="col-lg-6 col-xl-6 pt-5">
                            <a href="tel:<?php echo $telefones[0]['numero_telefone'] ?>" class="button-contact-modal" target="_blank">
                                <div class="floating-contacts-content">
                                    <h4>Ligar</h4>
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/img/tel.png"/>
                                </div>
                            </a>
                            <a href="https://api.whatsapp.com/send?phone=55<?php echo $grupoFooter['whatsapp'][0]['link_whatsapp'] ?>&text=Ol%C3%A1,%20tudo%20bem?" class="button-contact-modal" target="_blank">
                                <div class="floating-contacts-content">
                                    <h4>Whatsapp</h4>
                                    <img src="<?php bloginfo('template_directory'); ?>/assets/img/wpp.png"/>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="login_form">
                                <h3 class="text-white mb-4">Fale Conosco</h3>
                                <?php echo do_shortcode('[contact-form-7 title="Contato"]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Fale com um corretor -->
<div class="sign_up_modal modal fade bd-example-modal-lg-corretor d-none" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body container pb20">
                <div class="tab-content container" id="myTabContent">
                    <div class="row mt25 tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="col-lg-6 col-xl-6">
                            <div class="login_thumb">
                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/regstr2.jpg" class="img-modal-sumulacao"/>
                            </div>
                        </div>
                        <div class="col-lg-6 col-xl-6">
                            <div class="login_form">
                                <h2 class="text-center mb-4">Fale Com Um Corretor</h2>
                                <?php echo do_shortcode('[contact-form-7 title="Fale com um corretor"]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php wp_footer() ?>

</body>

</html>