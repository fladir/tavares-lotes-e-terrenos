<?php
/* Template Name: Blog */
get_header();
?>

    <section class="title-and-breadcrumb">
        <div class="container">
            <div class="row">
                <div class="col-12">
					<?php
					if ( function_exists( 'yoast_breadcrumb' ) ) {
						yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
					}
					?>
                    <h1 class="the_title mb-4">
						Blog
                    </h1>
                </div>
            </div>
        </div>
    </section>

    <!-- Main Blog Post Content -->
    <section class="blog_post_container bgc-f7">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <?php
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                    $args = array(
                        'nopaging' => false,
                        'paged' => $paged,
                        'post_type' => 'post',
                        'posts_per_page' => 6,
                        'orderby' => 'post_date',
                        'order' => 'DESC'
                    );

                    $WPQuery = new WP_Query($args);

                    ?>
                    <div class="row">
                        <?php if ($WPQuery->have_posts()) : while ($WPQuery->have_posts()) : $WPQuery->the_post(); ?>
                            <div class="col-lg-6">
                                <div class="for_blog feat_property">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="thumb">
                                            <?php the_post_thumbnail('img_post_list', array('class' => 'img-blog', 'alt' => '' . get_the_title() . '', 'title' => '' . get_the_title() . '')); ?>
                                            <div class="blog_tag">
                                                <?php
                                                $post_id = get_the_ID(); // or use the post id if you already have it
                                                $category_object = get_the_category($post_id);
                                                $category_name = $category_object[0]->name;
                                                echo $category_name;
                                                ?>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="details">
                                        <div class="tc_content">
                                            <a href="<?php the_permalink(); ?>">
                                                <h4><?php the_title() ?></h4>
                                            </a>
                                            <ul class="bpg_meta">
                                                <li class="list-inline-item"><a href="#"><i
                                                                class="flaticon-calendar"></i></a></li>
                                                <li class="list-inline-item"><a
                                                            href="#"><?php echo get_the_date() ?></a></li>
                                            </ul>
                                            <?php the_excerpt() ?>
                                        </div>
                                        <div class="fp_footer">
                                            <ul class="fp_meta float-left mb0">
                                                <li class="list-inline-item">
                                                    <a><?php echo get_avatar(get_the_author_meta('ID'), 100); ?></a>
                                                </li>
                                                <li class="list-inline-item">
                                                    <a><?php echo get_the_author_meta('display_name', $author_id); ?></a>
                                                </li>
                                            </ul>
                                            <a class="fp_pdate float-right text-thm" href="<?php the_permalink(); ?>">Leia
                                                Mais <span
                                                        class="flaticon-next"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endwhile;
                        endif;
                        wp_reset_query() ?>


                    </div>
                    <div class="row">



                        <div class="col-lg-12">
                            <?php echo bootstrap_pagination($WPQuery); ?>
                        </div>



                    </div>
                </div>
                <div class="col-lg-4">

                    <?php dynamic_sidebar('blog_sidebar'); ?>

                </div>
            </div>
        </div>
    </section>


<?php get_footer(); ?>