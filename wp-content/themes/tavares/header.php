<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<?php $grupo_header = get_field( 'grupo_header', 'options' ); ?>

	<?php if ( is_home() || is_front_page() ) : ?>

        <title><?php echo get_bloginfo( 'name' ) . ' | ' . get_bloginfo( 'description' ); ?></title>

	<?php else : ?>

        <title><?php wp_title( '|', true, 'right' ) ?></title>

	<?php endif; ?>

	<?php if ( get_field( 'grupo_header', 'options' )['favicon'] ): ?>
        <meta content="<?php echo get_field( 'grupo_header', 'options' )['favicon']; ?>" itemprop="image">
        <link href="<?php echo get_field( 'grupo_header', 'options' )['favicon']; ?>" rel="shortcut icon">
	<?php endif; ?>


    <!--/*Início Codigo Analytics*/-->
	<?php if ( get_field( 'analytics', 'options' ) ): ?>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
            ga('create', '<?php echo get_field( 'analytics', 'options' ); ?>', 'auto');
            ga('send', 'pageview');
        </script>
	<?php endif; ?>
    <!--/*Final Codigo Analytics*/-->


    <!--/*Início Codigo Head*/-->
	<?php if ( get_field( 'code_header', 'options' ) ): ?>
		<?php echo get_field( 'code_header', 'options' ); ?>
	<?php endif; ?>
    <!--/*Final Codigo Head*/-->

	<?php wp_head() ?>

    <style>
        :root {
        <?php if(get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_primaria']) : ?> --corPrimaria: <?php echo get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_primaria']; ?>;
        <?php endif; ?><?php if(get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_secundaria']) : ?> --corSecundaria: <?php echo get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_secundaria']; ?>;
        <?php endif; ?><?php if(get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['call_to_action']) : ?> --corTerciaria: <?php echo get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['call_to_action']; ?>;
        <?php endif; ?><?php if(get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['footer']) : ?> --corBody: <?php echo get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['footer']; ?>;
        <?php endif; ?><?php if(get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_titulos']) : ?> --corAuxiliar: <?php echo get_field('grupo_de_configuracoes_gerais', 'options')['cores_tema']['cor_titulos']; ?>;
        <?php endif; ?>
        }
    </style>
</head>

<body <?php body_class(); ?>>

<nav id="menu" class="stylehome1 mm-menu mm-menu_offcanvas" aria-hidden="true">
    <div class="mm-panels">
		<?php

		wp_nav_menu( array(
			'theme_location' => 'primary',
			'depth'          => 3,
			'container'      => 'div',
			'container_id'   => 'navbarNav',
			'menu_class'     => 'mm-listview',
			'fallback_cb'    => 'WP_Bootstrap_Navwalker::fallback',
			'walker'         => new WP_Bootstrap_Navwalker(),
		) );


		?>
</nav>
<div class="wrapper">
    <!--    <div class="preloader"></div>-->
    <!-- Main Header Nav -->
    <header class="header-nav menu_style_home_one style2 home3 navbar-scrolltofixed stricky main-menu <?php if ( is_front_page() ) : ?> position-absolute bg-transparent header-home <?php endif; ?> w-100">
        <div class="container p0 m0a ">
            <!-- Ace Responsive Menu -->
            <nav>
                <!-- Menu Toggle btn-->
                <div class="menu-toggle">
                    <img class="nav_logo_img img-fluid"
                         src="<?php bloginfo( 'template_directory' ); ?>/assets/images/header-logo.png"
                         alt="header-logo.png">
                    <button type="button" id="menu-btn">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <a href="<?php bloginfo( 'url' ); ?>" class="navbar_brand float-left dn-smd">
					<?php echo wp_get_attachment_image( get_field( 'grupo_header', 'options' )['logo_colorida'], 'logo' ); ?>

                </a>
                <a href="<?php bloginfo( 'url' ); ?>" class="navbar_brand_sticky float-left dn-smd d-none">
					<?php echo wp_get_attachment_image( get_field( 'grupo_header', 'options' )['logo_topo_fixo'], 'logo' ); ?>

                </a>
                <!-- Responsive Menu Structure-->
                <!--Note: declare the Menu style in the data-menu-style="horizontal" (options: horizontal, vertical, accordion) -->
				<?php

				wp_nav_menu( array(
				'theme_location' => 'primary',
				'depth' => 3,
				'container' => 'div',
				'container_id' => 'navbarNav',
				'menu_class' => 'ace-responsive-menu text-right',
				'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
				'walker' => new WP_Bootstrap_Navwalker(),
				) );


				?>

            </nav>

        </div>
    </header>

    <div id="page" class="stylehome1 h0">
        <div class="mobile-menu">
            <div class="header stylehome1">

                <div class="main_logo_home2 text-center">
                    <a href="<?php bloginfo( 'url' ); ?>" class="link-logo">
						<?php echo wp_get_attachment_image( get_field( 'grupo_header', 'options' )['logo_colorida'], 'logo' ); ?>
                    </a>
                </div>
                <ul class="menu_bar_home2">
                    <li class="list-inline-item list_s" style="opacity: 0"><a href="page-register.html"><span
                                    class="flaticon-user"></span></a></li>
                    <li class="list-inline-item"><a href="#menu"><span></span></a></li>
                </ul>
            </div>
        </div><!-- /.mobile-menu -->

    </div>




