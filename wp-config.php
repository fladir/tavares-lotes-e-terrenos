<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'tavares' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'ciaserver_user' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'ciaserver_mysql' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'yJ9|Zzc-dNv>qQ*rkIjm(J1s?$A_<Itx?Ah]ePp8|6+Z.7dSSEQytZji=P9!~{/o' );
define( 'SECURE_AUTH_KEY',  'b5eg40$24 wL15,}x7-7mo#G]SD.0Q(*fu9]dmP-3*9N1Ypc%#Bp(i8IftS{E (P' );
define( 'LOGGED_IN_KEY',    'h40]2^E:Cj@_[}FP[Vu}6atg|@M`(IF ,  _DZ;3m:=UrWV_HnrHd2Pw:Qb-|F!a' );
define( 'NONCE_KEY',        'LeBAFmb$w3lu4(1;;!T$_`[SUG<|_M,x2/=usv>Z6:h8L(#lK?C~NtP6OjAO<hV?' );
define( 'AUTH_SALT',        'R?|G+B?~uC`Fx_H?6D!wi^]!TKHvLz:T+@fuyRSa}Jfg^D<3|1&6utmuy~NHz1_I' );
define( 'SECURE_AUTH_SALT', '^PCeiVJ<ObB;nOSisjRvld38`;x8Yx t%p9J&P.61<29F,zA6kwz]ca8a6OyzL@U' );
define( 'LOGGED_IN_SALT',   '=-zX01kX11&;Q9t|Gt:%+}Udl@JF~O$ ;n7!=LWi?e(^3e#PI?hJH3r+cM^*JutD' );
define( 'NONCE_SALT',       'P*+5hs3Q}8MFr!YW5o^p&_T>qj~a,fhzlS{#qOR%X&R?zd?|kS}M=;YJkYiSIBcI' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'ltc_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';

/** Sets up 'direct' method for wordpress, auto update without ftp */
define('FS_METHOD','direct');
